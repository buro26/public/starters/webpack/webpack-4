const path = require('path')
const presetEnv = require("postcss-preset-env");
const autoprefixer = require("autoprefixer");
const chokidar = require( 'chokidar' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const { WebpackOpenBrowser } = require('webpack-open-browser')

const templatePath = 'path/to/my/template' // e.g. wp-content/themes/buro26
const devUrl = 'http://www.my-awesome-website.local'

module.exports = {
    mode: 'development',
    entry: {
        main: path.resolve(__dirname, `${templatePath}/ts/index.tsx`),
        styles: path.resolve(__dirname, `${templatePath}/scss/styles.scss`),
        vendors: path.resolve(__dirname, `${templatePath}/scss/vendors.scss`),
    },
    devtool: 'inline-source-map',
    output: {
        publicPath: 'http://localhost:9001/',
        filename: `js/[name].js`,
        path: path.resolve(__dirname, templatePath),
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.jsx', '.scss' ],
        alias: {
            'react-dom': '@hot-loader/react-dom',
        },
    },
    module: {
        rules: [
            {
                // Fix for framer motion v5: https://github.com/framer/motion/issues/1307#issuecomment-960991171
                test: /\.mjs$/,
                type: 'javascript/auto',
                include: /node_modules/,
            },
            {
                test: [/\.tsx?$/,  /\.jsx?$/],
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                "@babel/preset-typescript",
                                {
                                    allowNamespaces: true,
                                },
                            ],
                            "@babel/preset-react",
                            [
                                "@babel/preset-env",
                                {
                                    modules: false,
                                    targets: {
                                        esmodules: true,
                                    },
                                },
                            ],
                        ],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                            "@babel/plugin-syntax-dynamic-import",
                            "react-refresh/babel",
                        ],
                    }
                }
            },
            {
                test: [/\.tsx?$/,  /\.jsx?$/, /\.(s[ac]ss)$/],
                use: ["source-map-loader"],
                enforce: "pre",
            },
            {
                test: /\.(s[ac]ss)$/,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: true,
                            sourceMap: true,
                            publicPath: (resourcePath, context) => {
                                return '/';
                            },
                        },
                    },
                    // Translates CSS into CommonJS
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                        },
                    },
                    // Resolve urls
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
                    // Compiles Sass to CSS
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                outputStyle: 'compressed',
                            },
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                parser: 'postcss-scss',
                                plugins: [
                                    "autoprefixer",
                                    presetEnv({}),
                                    autoprefixer({
                                        grid: 'autoplace',
                                    }),
                                ],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: true,
                            sourceMap: true,
                            publicPath: (resourcePath, context) => {
                                return '/';
                            },
                        },

                    },
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                parser: 'postcss-scss',
                                plugins: [
                                    "autoprefixer",
                                    presetEnv({}),
                                    autoprefixer({
                                        grid: 'autoplace',
                                    }),
                                ],
                            },
                        },
                    },
                ],
            },
            {
                // File loader for images
                test: /\.(svg|jpg|jpeg|png)(?:[?#].+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path]/[name].[ext]',
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'css/[name].css',
            chunkFilename: '[id].css',
        }),
        new ReactRefreshWebpackPlugin({
            forceEnable: true,
            include: [path.resolve(__dirname, '/*')]
        }),
        new WebpackOpenBrowser({ url: `${devUrl}`}),
    ],
    devServer: {
        open: false,
        hot: true,
        contentBase: path.join(__dirname, 'http://localhost:9001/'),
        compress: true,
        port: 9001,
        disableHostCheck: true,
        headers: {'Access-Control-Allow-Origin': '*'},
        historyApiFallback: true,
        /**
         * Watch for changes to PHP files and reload the page when one changes.
         */
        before ( app, server ) {
            const files = [
                path.resolve(__dirname, `${templatePath}/**/*.php`),
                // path.resolve(__dirname, 'wp-content/plugins/buro26-hot-reload-on-post-save/last-update.json'), // optional usage of hot-reload-on-post-save plugin
            ];

            chokidar
                .watch( files, {
                    alwaysStat: true,
                    atomic: false,
                    followSymlinks: false,
                    ignoreInitial: true,
                    ignorePermissionErrors: true,
                    persistent: true,
                    usePolling: true
                } )
                .on( 'all', () => {
                    server.sockWrite( server.sockets, "content-changed" );
                } );
        },

    }
};