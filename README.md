# Webpack 4 (deprecated)

> This repository is deprecated. Please use Webpack 5 for new projects.

Template for a simple template for usage within Joomla or WordPress with Webpack 4. Use the [base template](https://gitlab.com/buro26/public/starters/webpack/webpack-4-styles-only) for a template that only requires sass compilation and reloading for a smaller footprint.

## Features
- Compile scss
- Compile Typescript (ES6)
- Compile React
- Hot Module Replacement

## Getting started
Copy files in root directory of your project. Run `npm install` to install all dependencies. 

Run `npm run start` to start the development server. Run `npm run build` to build the project for production.

### Commands
**Start the development server**
```bash
npm run start
```

**Build files for production**
```bash
npm run build
```

## Configuration
### Webpack
[Webpack's configuration](https://v4.webpack.js.org/configuration/) is split into two files, webpack-dev-config.js and webpack-prod-config.js. The development configuration is used when running `npm run start` and the production configuration is used when running `npm run build`.

The dev config is set up to use the [webpack-dev-server](https://v4.webpack.js.org/configuration/dev-server/).

### Typescript
[Typescript's configuration](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html) is set up in tsconfig.json. The configuration is set up to use the [ES6 module system](https://www.typescriptlang.org/docs/handbook/modules.html).


## Main packages used
- [Webpack](https://webpack.js.org/)
- [Webpack Dev Server](https://webpack.js.org/configuration/dev-server/)
- [Redux](https://redux.js.org/)
- [React](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Sass](https://sass-lang.com/)
- [Babel](https://babeljs.io/)
- [PostCSS](https://postcss.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [React Router](https://reactrouter.com/)
- [React Redux](https://react-redux.js.org/)
- [Apollo Client](https://www.apollographql.com/docs/react/)
- [React Helmet](https://www.npmjs.com/package/react-helmet)
- [Axios](https://www.npmjs.com/package/axios)